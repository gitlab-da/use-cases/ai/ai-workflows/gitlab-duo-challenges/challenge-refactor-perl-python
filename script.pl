#!/usr/bin/perl
use strict;
use warnings;

open my $md_fh, '<', 'file.md' or die "Could not open file.md: $!";

my $l = 0;
my $e = 0;
my $h = 0;

while (my $line = <$md_fh>) {
  $l++;
  if ($line =~ /^\s*$/) {
    $e++;
    next;
  }
  if ($line =~ /^#+\s*(.+)/) {
    print "$1\n";
    $h++; 
  }
}

print "\nS:\n"; 
print "L: $l\n";
print "E: $e\n"; 
print "H: $h\n";